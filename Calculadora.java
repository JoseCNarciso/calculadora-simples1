import java.util.Scanner;
import java.util.function.DoubleBinaryOperator;

public class Calculadora {
    public static void main(String[] args) {
        System.out.println("CALCULADORA SIMPLES" );

        int opcao;
        do {

            System.out.println("1 - Somar ");
            System.out.println("2 - Subtrair" );
            System.out.println("3 - Multiplicar" );
            System.out.println("4 - Dividir" );
            System.out.println("O que você deseja fazer (0 para sair): ");
            
            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();

            
            
            processar(opcao);
        }while ( opcao != 0);
    }

    public static void processar(int opcao) {
        switch(opcao) {
            case 1: {
                Scanner scanner = new Scanner(System.in);
                System.out.println("SOMANDO DOIS NUMEROS");
                
                System.out.print(" Digite o primeiro número: ");
                int numero1 = scanner.nextInt();
                
                System.out.println(" Digite o segundo número: ");
                int numero2 = scanner.nextInt();
                int soma =  numero1 + numero2 ;

                System.out.println("A soma dos dois numeros é : " + soma );
                break;              
            }
            case 2:{
                Scanner scanner = new Scanner(System.in);
                System.out.println("SUBTRAINDO DOIS NUMEROS");
                
                System.out.print(" Digite o primeiro número: ");
                int numero1 = scanner.nextInt();
                
                System.out.println(" Digite o segundo número: ");
                int numero2 = scanner.nextInt();
                int resultado =  numero1 - numero2 ;

                System.out.println("A subtração  dos dois numeros é : " + resultado) ;
                break;              
            }
            case 3:{
                Scanner scanner = new Scanner(System.in);
                System.out.println("MULTIPLICANDO DOIS NUMEROS");
                
                System.out.print(" Digite o primeiro número: ");
                int numero1 = scanner.nextInt();
                
                System.out.println(" Digite o segundo número: ");
                int numero2 = scanner.nextInt();
                int resultado =  numero1 * numero2 ;

                System.out.println("O produto  dos dois numeros é : " + resultado) ;
                break;              
            }
            case 4:{
                Scanner scanner = new Scanner(System.in);
                System.out.println("DIVIDINDO DOIS NUMEROS");
                
                System.out.print(" Digite o primeiro número: ");
                double numero1 = scanner.nextInt();
                
                System.out.println(" Digite o segundo número: ");
                double numero2 = scanner.nextInt();
                if (numero2 == 0) {
                    System.out.println("Impossível dividir por 0 ");
                } else{
                    double resultado =  numero1 / numero2 ;
                    System.out.println("O produto  dos dois numeros é : " + resultado) ;
                } 
                break;
            }
        }
    }

}
 
